const express = require('express');

const port = 8080;

const app = express();

app.use(express.json());

let items = [
    {
        name: "Mjolnir",
        price: 50000,
        isActive: true
    },
    {
        name: "Vibranium Shield",
        price: 70000,
        isActive: true
    }

];


// HOME
app.get("/home", (request, response) => {
	response.status(201).send("Welcome to the home page")
})


// ITEMS
app.get("/items", (request, response)=> {
		response.send(items);
})


// DELETE
app.delete("/delete-item", (request, response) => {
	let deletedItem = items.pop();
	response.send(deletedItem);
})


app.listen(port, () => console.log(`Server is running at port ${port}.`))